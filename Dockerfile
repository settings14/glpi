FROM php:8.2-fpm

# Arguments defined in docker-compose.yml
ARG user
ARG uid

# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    libldap2-dev \
    libldb-dev \
    libssl-dev \
    libressl-dev

RUN apt-get install -y zip unzip bzip2

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Timezone
RUN apt-get install tzdata
ENV TZ=Europe/Kyiv

# PHP extensions possible values
#bcmath bz2 calendar ctype curl dba dl_test dom enchant exif ffi fileinfo filter ftp gd gettext gmp hash 
#iiconv imap intl json ldap mbstring mysqli oci8 odbc opcache pcntl 
#pdo pdo_dblib pdo_firebird pdo_mysql pdo_oci pdo_odbc pdo_pgsql pdo_sqlite pgsql phar posix pspell 
#readline reflection session shmop simplexml snmp soap sockets sodium spl standard sysvmsg sysvsem 
#sysvshm tidy tokenizer xml xmlreader xmlwriter xsl zend_test zip

# Install Mandatory PHP extensions
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install mbstring
RUN docker-php-ext-install exif
RUN docker-php-ext-install pcntl
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install gd
#RUN docker-php-ext-install curl
RUN docker-php-ext-install fileinfo
#RUN docker-php-ext-install json
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install session
#RUN docker-php-ext-install zlib
RUN docker-php-ext-install simplexml
RUN docker-php-ext-install xml
RUN docker-php-ext-install intl

# Install Optional PHP extensions
#RUN docker-php-ext-install cli
RUN docker-php-ext-install dom
#RUN docker-php-ext-install bz2 zip opcache
RUN docker-php-ext-install opcache
#RUN docker-php-ext-install openssl

# Install LDAP system support
RUN docker-php-ext-configure ldap \
    && docker-php-ext-install ldap


# Get latest Composer
#COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Create system user to run Composer
RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user

# FSH folder for GLPi
#RUN mkdir -p /var/log/glpi && chown -R $user:$user /var/log/glpi
#RUN mkdir -p /var/lib/glpi && chown -R $user:$user /var/lib/glpi
#RUN mkdir -p /etc/glpi && chown -R $user:$user /etc/glpi
#COPY ./inc/local_define.php /etc/glpi/local_define.php

# Set working directory
WORKDIR /var/www/glpi/public
#RUN mkdir -p /var/www/files && chown -R $user:$user /var/www/files && chmod -R ug+w /var/www/files
#RUN chmod -R ug+w /var/www/files/

USER $user
